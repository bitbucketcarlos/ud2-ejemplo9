# Ud2-Ejemplo9
_Ejemplo 9 de la Unidad 2._ 

Vemos un ejemplo de uso de _ConstraintLayout_ que se ha implementado usando el editor de diseño de _AndroidStudio_. En él podéis ver las restricciones utilizadas.

_Imagen de material.io/tools/icons (Licencia Apache 2.0)_